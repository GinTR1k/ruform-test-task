import asyncio
import logging

from aiohttp import web

from application.config import Config
from application.handlers import UserHandler
from application.models import db
from application.routes import setup_routes


app = web.Application()


async def init():
    handler = UserHandler()
    setup_routes(app, handler)


async def setup_db(config):
    return await db.set_bind(config.DATABASE_URL)


def main(config=Config):
    logging.basicConfig(level=logging.DEBUG)
    loop = asyncio.get_event_loop()

    loop.run_until_complete(init())
    app['db'] = loop.run_until_complete(setup_db(config))

    web.run_app(app, host=config.HOST, port=config.PORT)


if __name__ == '__main__':
    main()
