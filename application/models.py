from gino import Gino


db = Gino()


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, index=True)

    def as_dict(self) -> dict:
        return {'id': self.id, 'name': self.name}
