def setup_routes(app, handler):
    router = app.router
    h = handler
    router.add_get('/users', h.find_users, name='find_users')
    router.add_post('/users', h.create_user, name='create_user')
