from aiohttp import web

from application.exceptions import JsonValidaitonError
from application.models import User


class UserHandler:
    async def find_users(self, request):
        try:
            name = request.rel_url.query['name']
        except Exception:
            raise JsonValidaitonError()

        users = await User.query.where(User.name.ilike(f'%{name}%')).gino.all()

        return web.json_response({'users': [user.as_dict() for user in users]})

    async def create_user(self, request):
        try:
            data = await request.json()
            name = data['name']
        except Exception:
            raise JsonValidaitonError()

        if not name:
            raise JsonValidaitonError('Username is empty')

        if await User.query.where(User.name.ilike(name)).gino.first():
            raise JsonValidaitonError('User already exists')

        user = await User.create(name=name)

        return web.json_response({'user': user.as_dict()})
