from environs import Env

env = Env()
env.read_env()


class Config:
    HOST = env.str('HOST', '0.0.0.0')
    PORT = env.int('HOST', 27015)
    DATABASE_URL = env.str('DATABASE_URL')
