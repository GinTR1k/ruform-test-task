run:
	PYTHONPATH=. python application/main.py

create_revision:
	PYTHONPATH=. alembic revision --autogenerate

upgrade_revision:
	PYTHONPATH=. alembic upgrade head
